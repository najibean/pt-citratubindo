import { JetView } from "webix-jet";
import { doLogin, user } from "../models/records";

let form = {
  view: "form",
  id: "form_iki",
  hidden: true,
  elements: [
    { view: "text", label: "Email", name: "username" },
    { view: "text", type: "password", label: "Password", name: "password" },
    {
      cols: [
        {
          view: "button",
          click: function () {
            const values = $$("form_iki").getValues();
            console.log("values", values);
            doLogin(values).then((res) => {
              localStorage.setItem("tokenLogin", res.access_token);
              user.token = res.access_token;
              $$("form_iki").hide();
              $$("btn_logout").show();
            });
          },
          value: "Login",
          css: "webix_primary",
        },
      ],
    },
  ],
};

let logout = {
  id: "btn_logout",
  view: "button",
  hidden: true,
  click: function () {
    localStorage.clear();
    user.token = null;
    $$("form_iki").show();
    $$("btn_logout").hide();
  },
  value: "Logout",
  css: "webix_primary",
};

export default class LoginView extends JetView {
  config() {
    return {
      rows: [form, logout],
    };
  }
  init() {}
  ready() {
    if (user.token) {
      $$("form_iki").hide();
      $$("btn_logout").show();
    } else {
      $$("form_iki").show();
      $$("btn_logout").hide();
    }
  }
}
