import { JetView, plugins } from "webix-jet";
import { user } from "../models/records";

export default class TopView extends JetView {
  config() {
    var header = {
      type: "header",
      template: this.app.config.name,
      css: "webix_header app_header",
    };

    var menu = {
      view: "menu",
      id: "top:menu",
      css: "app_menu",
      width: 180,
      layout: "y",
      select: true,
      template: "<span class='webix_icon #icon#'></span> #value# ",
      data: [
        { value: "user", id: "user", icon: "wxi-user" },
        { value: "Data", id: "data", icon: "wxi-pencil" },
      ],
    };

    var ui = {
      type: "clean",
      paddingX: 5,
      css: "app_layout",
      cols: [
        {
          paddingX: 5,
          paddingY: 10,
          rows: [{ css: "webix_shadow_medium", rows: [header, menu] }],
        },
        { type: "wide", paddingY: 10, paddingX: 5, rows: [{ $subview: true }] },
      ],
    };

    return ui;
  }
  init() {
    let getStorage = localStorage.getItem("tokenLogin");
    if (getStorage) {
      user.token = getStorage;
    }
    this.use(plugins.Menu, "top:menu");
  }

}


