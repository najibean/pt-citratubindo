import { JetView } from "webix-jet";
import { addMovie, deleteMovie, getMovies, user } from "../models/records";

let dbNotLogin = {
  template: "Anda Harus Login Dulu",
  css: { "text-align": "center", "padding-top": "100px" },
};

let formAdd = {
  view: "window",
  id: "addForm",
  head: "Add Movie",
  position: "center",
  width: 500,
  height: 500,
  body: {
    view: "form",
    id: "form_add_iki",
    elements: [
      { view: "text", label: "Title", name: "title" },
      { view: "textarea", label: "Synopsis", name: "synopsis" },
      { view: "text", label: "Release", name: "releaseDate" },
      {
        cols: [
          {
            view: "button",
            click: function () {
              const values = $$("form_add_iki").getValues();
              // console.log("values", values);
              addMovie(values, user.token).then((res) => {
                // console.log("res", res);
                getMovies().then((res) => {
                  $$("db_iki").parse(res);
                });
                $$("addForm").destructor();
              });
            },
            value: "Add",
            css: "webix_primary",
          },
          {
            view: "button",
            value: "Cancel",
            click: function () {
              $$("addForm").destructor();
            },
          },
        ],
      },
    ],
  },
};

let db = {
  view: "datatable",
  id: "db_iki",
  columns: [
    {
      id: "title",
      header: ["Film title"],
      width: 250,
    },
    { id: "synopsis", header: ["Synopsis"], adjust: true },
    { id: "releaseDate", header: ["Release"], width: 200 },
  ],
  select: "row",
};

let tb = {
  view: "toolbar",
  id: "tb_iki",
  hidden: true,
  cols: [
    {
      view: "button",
      value: "Add",
      width: 100,
      align: "left",
      click: function () {
        this.$scope.ui(formAdd).show();
      },
    },
    {
      view: "button",
      value: "Delete",
      width: 100,
      align: "center",
      click: function () {
        const item = $$("db_iki").getSelectedItem();
        console.log("item", item);
        deleteMovie(item.id, user.token).then(() => {
          $$("db_iki").remove(item.id);
        });
      },
    },
  ],
};

export default class DataView extends JetView {
  config() {
    return {
      rows: [tb, user.token ? db : dbNotLogin],
    };
  }
  init() {
    getMovies().then((res) => {
      $$("db_iki").parse(res);
      $$("tb_iki").show();
    });
  }
  ready() { }
  destructor() { }
}
