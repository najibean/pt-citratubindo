export let user = {
  token: null,
};

export const doLogin = (dataLogin) => {
  return webix
    .ajax()
    .post("https://gentle-garden-05760.herokuapp.com/users/login", dataLogin)
    .then((res) => res.json());
};

export const register = (register) => {
  return webix
    .ajax()
    .post("https://gentle-garden-05760.herokuapp.com/users/register", register)
    .then((res) => res.json());
};

export const addMovie = (dataMovie, token) => {
  return webix
    .ajax()
    .headers({
      "Content-type": "application/json",
      access_token: token,
    })
    .post(`https://gentle-garden-05760.herokuapp.com/movies/add`, dataMovie)
    .then((res) => res.json());
};

export const getMovies = () => {
  return webix
    .ajax("https://gentle-garden-05760.herokuapp.com/movies/")
    .then((res) => res.json());
};

export const deleteMovie = (id, token) => {
  return webix
    .ajax()
    .headers({
      "Content-type": "application/json",
      access_token: token,
    })
    .del(`https://gentle-garden-05760.herokuapp.com/movies/delete/${id}`)
    .then((res) => res.json());
};
