Webix Jet Demo App
===================
### Hi Mr. HerSam,
#### please use this user to test the app :
```
email       = najib@email.com
password    = 123
```
#
#### I use the url of my previous project to fetch the data to make READ, ADD, DELETE. So sorry I left behind UPDATE due to not enough time :D

#

### How to run

- run ```npm install```
- run ```npm start```
- open ```http://localhost:8080```

For more details, check https://www.gitbook.com/book/webix/webix-jet/details

### Other commands

#### Run lint

```
npm run lint
```

#### Build production files

```
npm run build
```

After that you can copy the "codebase" folder to the production server


### License

MIT